require 'rubygems'
require 'asin'
require 'awesome_print'

ASIN::Configuration.configure do |config|
  config.associate_tag = 'carlscorn-20'
  config.key           = 'AKIAINHECWH7WODJB77A'
  config.secret        = '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'
end

client = ASIN::Client.instance
# Book best sellers
nodes_to_traverse = ['7728816011']
collected_nodes = []

while nodes_to_traverse.any?
  resp = client.browse_node(nodes_to_traverse[0]).first.children.map(&:BrowseNodeId)
  nodes_to_traverse.push(*resp)
  
  nodes_to_traverse = nodes_to_traverse.drop(1)

  collected_nodes.push(*resp)
  10.times do
    puts nodes_to_traverse.uniq.count
    puts collected_nodes.uniq.count
  end

  nodes_to_traverse = nodes_to_traverse.uniq
  sleep rand() + 0.7
end


puts "\n\n ========== \n\n"
ap "Uniques:  #{collected_nodes.uniq.count}"
ap "Total:    #{collected_nodes.uniq.count}"
puts "\n\n ========== \n\n"
percent_unique = (collected_nodes.uniq.count / collected_nodes.count).to_i
ap "Filtered: %#{percent_unique}"

# throw em all in a file for later
File.open('book_best_sellers_nodes.txt', 'w') do |nodes_file|
  collected_nodes.uniq.each do |node_id|
    nodes_file.puts node_id
  end
end
