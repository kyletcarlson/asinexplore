class Connector
  
  def initialize
    ASIN::Configuration.configure do |config|
      config.associate_tag = 'carlscorn-20'
      config.key           = 'AKIAINHECWH7WODJB77A'
      config.secret        = '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'
    end
    @asin_client = ASIN::Client.instance
  end

  def pluck_asins(item_set)
    asin_batch = []

    item_set.each do |p|
      begin
        if p.is_a?(Array)
          asin_batch << p[1]
        else
          asin_batch << p.ASIN
        end
      rescue
        nil
      end
    end

    asin_batch
  end

  # Give it an array of nodes, and get back a flattened array the BrowseNodeIds of all of its children
  def collect_product_node_ids(nodes_to_traverse)
    collected_nodes = []

    while nodes_to_traverse.any?
      nodes_to_traverse.first[1].each do |node|
        collected_nodes << node.BrowseNodeId
        nodes_to_traverse = nodes_to_traverse.drop(1)

        if node.has_key?('Children')
          node.Children.first[1].each do |x|
            puts x.BrowseNodeId
            collected_nodes << x.BrowseNodeId
            nodes_to_traverse << x.BrowseNodeId
            nodes_to_traverse = nodes_to_traverse.drop(1)
          end
        end
      end
    end

    collected_nodes
  end
  
  # Exactly the same item lookup as what ASIN provides, but this leaves room for logging/tracking/validation/etc.
  def lookup_by_asin(asin)
    item = @asin_client.lookup(asin, ResponseGroup: :Large).first
  end

  # Updates or creates a new product if we can't find one with the ASIN
  def create_or_update(product_params)
    product = Product.where("asin = ?", product_params[:asin])
    if product.empty?
      Product.create!(product_params)
    else
      product.first.update_attributes(product_params)
    end
  end

  # Takes an array of ASINs, creates/updates the records, 
  def fetch_batch(asins_to_fetch)
    product_batch = []

    fetched_products = @asin_client.lookup(asins_to_fetch.take(10))

    fetched_products.each_with_index do |p, index|
      
      p = p.raw.BrowseNodes.first

      if p[1].first.is_a?(Array)
        node_id = p[1].first[1]
      else
        node_id = p[1][0].BrowseNodeId
      end

      if VALID_NODES.include?(node_id)
        product_params = self.create_params(fetched_products[index])
        self.create_or_update(product_params)
      end
    end

    product_batch
  end


  #
  # ADMIN METHODS
  #

  # Used to manually update a product while on that page.
  def fetch_product(asin)
    item = @asin_client.lookup(asin, ResponseGroup: :Large).first
    product_params = self.create_params(item)
    self.create_or_update(product_params) 
  end

  # Same as fetch_batch(), but it doesn't return anything and doesn't check VALID_NODES constraints. Used for initial database seeding.
  def create_batch(asins_to_fetch)
    fetched_products = @asin_client.lookup(asins_to_fetch.take(10), ResponseGroup: :Large)

    fetched_products.each do |p|
      node_id = p.raw.BrowseNodes.first[1][0].BrowseNodeId

      product_params = self.create_params(p)
      self.create_or_update(product_params)
    end
  end
end