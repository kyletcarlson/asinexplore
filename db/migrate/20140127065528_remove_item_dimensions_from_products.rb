class RemoveItemDimensionsFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :item_height, :string
    remove_column :products, :item_weight, :string
    remove_column :products, :item_length, :string
    remove_column :products, :item_width, :string
  end
end
