class SearchesController < ApplicationController

  skip_authorization_check
  
  def new
    @search = Search.new
  end

  def create
    @search = Search.create!(search_params)
    redirect_to @search
  end

  def show
    @search = Search.find(params[:id])
    @client = ASIN::Client.instance
    resp = @client.search({ Keywords: @search.keywords, SearchIndex: :Books, ResponseGroup: :Small })
    @more_url = resp['ItemSearchResponse']['MoreSearchResultsUrl']
    
    asin = resp['ItemSearchResponse']['Items'][0].asin
    resp = @client.similar(asin, ResponseGroup: :Small, SimilarityType: :Intersection)
    if resp.any?
      related_asins = resp.collect { |p| p.raw.ASIN }

      # see if there are any items we need to pull from Amazon
      @products = Product.where(asin: related_asins)
      existing_asins = @products.pluck(:asin)
      asins_to_fetch = related_asins.delete_if { |a| existing_asins.include?(a) }

      if asins_to_fetch.any?
        conn = Connector.new
        fetched_batch = conn.fetch_batch(asins_to_fetch)
        # @products << fetched_batch
        @products.all.push(*fetched_batch)
      end

      @products
    end

    @products
  end

  def update
  end

  private
    def search_params
      params.require(:search).permit(:keywords, :min_price, :max_price)
    end
end
