class UserMailer < ActionMailer::Base
  default :from => "welcome@example.com"
 
  def welcome_email(user)
    @user = user
    mail(:to => user.email,
    :bcc => "keymaster@example.com",
    :subject => "Welcome to #{APP_NAME}. You'll love it!")
  end
end