module LockersHelper

  def locker_product_image(locker)
    if locker.lockups.any?
      @lockup = locker.lockups.first
    else
      @lockup = Lockup.all.order("RAND()").first
    end
  end
end
