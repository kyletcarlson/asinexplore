class Product < ActiveRecord::Base
  validates :asin, presence: true
  validates :name, :presence => true
  validates_numericality_of :price, :allow_nil => false, :greater_than_or_equal_to => 0

  def fetch_info
    asin_client = ASIN::Client.instance
    item = asin_client.lookup(self.asin, ResponseGroup: :Large).first
    product_params = Product.create_params(item)
    self.update_attributes(product_params)
  end

  def self.fetch_batch(asins)
    asin_client = ASIN::Client.instance
    items = asin_client.lookup(asins, ResponseGroup: :Large)
    items
  end

  def self.create_params(item)
    default_url = 'http://www.amazon.com/Best-Sellers-Books-Computer-Programming/zgbs/books/3839/?_encoding=UTF8&camp=1789&tag=carlscorn-20&creative=390957&linkCode=ur2'
    item = item[0] if item.is_a?(Array)

    product_params = {
      asin: item.asin, 
      name: item.raw.ItemAttributes.Title,
      sales_rank: item.sales_rank,
      author: '',
      binding: item.binding,
      ean: item.ean,
      edition: item.edition,
      isbn: item.raw.ItemAttributes.ISBN,
      label: item.label,
      language: 'English',
      manufacturer: item.manufacturer,
      mpn: item.raw.ItemAttributes.MPN,
      page_count: item.page_count,
      part_number: item.part_number,
      product_group: item.product_group,
      product_type_name: item.raw.ItemAttributes.ProductTypeName,
      publication_date: item.publication_date,
      publisher: item.publisher,
      sku: item.raw.ItemAttributes.SKU,
      studio: item.studio,
      url: item.raw.DetailPageURL
    }

    if item.raw.ItemAttributes.ReleaseDate
      product_params[:release_date] = item.raw.ItemAttributes.ReleaseDate
    end

    if item.raw.ItemAttributes.ListPrice
      product_params[:price] = (item.raw.ItemAttributes.ListPrice.Amount.to_d / 100)
    else
      product_params[:price] = 0
    end

    if item.raw.ImageSets.ImageSet.is_a?(Array)
      product_params[:image_url] = item.raw.ImageSets.first[1][0].LargeImage.URL
    else
      product_params[:image_url] = item.raw.ImageSets.ImageSet.LargeImage.URL
    end

    if item.raw.ItemAttributes.Author
      if item.raw.ItemAttributes.Author.kind_of?(Array)
        product_params[:author] = ""
        item.raw.ItemAttributes.Author.each do |a|
          product_params[:author] << a + ", "
        end
        product_params[:author] = product_params[:author][0..-3]
      else
        product_params[:author] = item.raw.ItemAttributes.Author
      end
    end

    if item.raw.OfferSumamry
      prouct_params[:total_new] = item.raw.OfferSummary.TotalNew
      prouct_params[:total_used] = item.raw.OfferSummary.TotalUsed
    end

    if item.raw.EditorialReviews.EditorialReview.is_a?(Array)
      item.raw.EditorialReviews.EditorialReview.each do |review|
        if review.Source == "Product Description"
          product_params[:description] = review.Content
        else
          product_params[:amazon_review] = review.Content
        end
      end
    else
      product_params[:description] = item.raw.EditorialReviews.EditorialReview.Content
    end

    product_params
  end

  def self.create_or_update(product_params)
    product = Product.where("asin = ?", product_params[:asin])
    if product.empty?
      Product.create!(product_params)
    else
      product.first.update_attributes(product_params)
    end
  end
end