module ApplicationHelper

 def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  # Returns the full title on a per-page basis.
  def full_title(page_title)
    if page_title.empty?
      "#{APP_NAME} - #{APP_TITLE_DESCRIPTION}"
    else
      "#{page_title} - #{APP_NAME}"
    end
  end

  # Returns the meta description on a per-page basis.
  def meta_description(page_descripton)
    if page_descripton.empty?
      "#{APP_META_DESCRIPTION}"
    else
      "#{page_descripton}"
    end
  end

  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    clear_return_to
  end

  def store_location
    session[:return_to] = request.fullpath
  end

  private

    def clear_return_to
      session.delete(:return_to)
    end
    
end
