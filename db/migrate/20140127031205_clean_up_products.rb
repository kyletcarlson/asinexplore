class CleanUpProducts < ActiveRecord::Migration
  def change
    add_column :products, :amazon_review, :text
    add_column :products, :product_type_name, :string
    add_column :products, :release_date, :string

    change_column :products, :url, :text
    change_column :products, :name, :text

    remove_column :products, :formatted_price, :string
  end
end
