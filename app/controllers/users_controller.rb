class UsersController < ApplicationController

  before_action :authenticate_user!, :except => [:show, :lockups, :loves, :hates]
  skip_authorization_check :only => [:show, :lockups, :loves, :hates]

  respond_to :html

  def index
    authorize! :index, User, :message => "Sorry, you're not an administrator."
    @users = User.all
    redirect_to root_path
  end

  def show
    @user = User.find(params[:id])
  end
end
