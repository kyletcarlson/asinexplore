class ProductsController < ApplicationController

  before_action :initialize_asin, only: [:show, :seed]
  skip_authorization_check #only: [:index, :show, :buy, :search, :seed]

  respond_to :html, :json

  def index
    @products = Product.all.order("RAND()").limit(25)
    respond_with @products
  end

  def buy
    @product = Product.find(params[:id])
    redirect_to @product.url
  end

  def show
    authorize! :show, Product
    @product = Product.find(params[:id])
    @meta_description = @product.description[0..140]

    # grab customer reviews <iframe> url
    resp = @client.lookup(@product.asin, ResponseGroup: :Reviews).first
    @amazon_reviews = resp.raw.CustomerReviews.first[1]

    # grab the items related to the current product
    resp = @client.similar(@product.asin, ResponseGroup: :Small, SimilarityType: :Random)
    if resp.any?
      related_asins = resp.collect { |p| p.asin }

      # see if there are any items we need to pull from Amazon
      @related_products = Product.where(asin: related_asins)
      existing_asins = @related_products.pluck(:asin)
      asins_to_fetch = related_asins.delete_if { |a| existing_asins.include?(a) }

      # create each product we fetched that doesn't already exist
      if asins_to_fetch.any?
        # call to Amazon & return the items from the ItemLookup request
        fetched_items = Product.fetch_batch(asins_to_fetch)
        # loop through the items, create product params, then create/update each item
        [fetched_items].each do |p|
          product_params = Product.create_params(p)
          Product.create_or_update(product_params)
        end
      end
      
      @related_products = Product.all.where("asin IN (?)", existing_asins)
    end
    
    respond_with @product
  end

  def fetch
    @product = Product.find(params[:id])
    @product.fetch_info
    redirect_to @product
  end

  def seed
    # authorize! :manage, Product
    items_to_process = []

    VALID_NODES.shuffle!.each do |n|
      resp = @client.browse_node(n, :ResponseGroup => [:BrowseNodeInfo, :NewReleases, :TopSellers, :MostWishedFor, :MostGifted]).first
      items_to_process  = []

      if resp.raw.TopItemSet
        resp.raw.TopItemSet.each do |set|
          items_to_process.push(*resp.raw.TopItemSet.TopItem.collect(&:ASIN))
        end
      end

      items_to_process = items_to_process.uniq

      puts "\n\n========== \n\n"

      items_to_process.each_slice(10) do |batch|
        fetched_items = Product.fetch_batch(batch)

        fetched_items.each do |p|
          product_params = Product.create_params(p)
          Product.create_or_update(product_params)
        end

        sleep rand() + 0.8
      end
    end

    redirect_to root_path
  end


  def destroy
    authorize! :manage, Product
    @product = Product.find(params[:id])
    @product.destroy
    flash[:notice] = "Product deleted!" if @product.destroy
    redirect_to root_path
  end

  private

    def initialize_asin
      @client = ASIN::Client.instance
    end

    def product_params
      params.require(:product).permit(:description, :image_url, :name, :price, :sku, :asin)
    end
end
