Asinexplore::Application.routes.draw do

  # use this line for regular email/password registration
  devise_for :users, :controllers => { :registrations => "registrations" }
  resources :users

  resources :products do
    member do
      get :buy
      get :fetch
    end

    collection do
      get :seed
    end
  end

  resources :searches


  namespace :api, path: "", defaults: { format: :json }, constraints: { subdomain: "api" } do
    resources :products
  end

  get "sitemap.xml" => "home#sitemap", format: :xml, as: :sitemap
  get "robots.txt" => "home#robots", format: :text, as: :robots

  root :to => 'products#index'
end
