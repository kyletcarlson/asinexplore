# == Schema Information
#
# Table name: products
#
#  id               :integer          not null, primary key
#  asin             :string(20)
#  name             :string(255)
#  description      :text
#  price            :float
#  url              :string(255)
#  image_url        :string(255)
#  sales_rank       :integer
#  author           :text
#  binding          :string(255)
#  ean              :string(255)
#  edition          :string(255)
#  isbn             :string(255)
#  item_height      :string(255)
#  item_length      :string(255)
#  item_weight      :string(255)
#  item_width       :string(255)
#  label            :string(255)
#  language         :string(255)
#  formatted_price  :string(255)
#  manufacturer     :string(255)
#  mpn              :string(255)
#  page_count       :string(255)
#  part_number      :string(255)
#  product_group    :string(255)
#  publication_date :string(255)
#  publisher        :string(255)
#  sku              :string(255)
#  studio           :string(255)
#  total_new        :string(255)
#  total_used       :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

require 'spec_helper'

describe Product do
  it "has a valid factory" do
    product = Product.new(name: "asdf", asin: "0321584104", price: 12.32)
    expect(product.save).to be_true
  end

  before(:each) do
    @product = build(:product)
  end

  it { should respond_to(:fetch_info) }

  # it "initializes with proper params" do
  #   expect(ASIN::Configuration.associate_tag).to eq(ENV['ASIN_TAG'])
  #   expect(ASIN::Configuration.key).to eq(ENV['ASIN_KEY'])
  #   expect(ASIN::Configuration.secret).to eq(ENV['ASIN_SECRET'])
  # end

  # it "retrieves a book", :vcr do
  #   item = @connector.lookup_by_asin('0321584104')
  #   expect(item.asin).to eq('0321584104')
  #   expect(item.title).to start_with('Eloquent Ruby')
  #   expect(item.amount.to_i).to be >= 0
  #   expect(item.raw.ItemAttributes).to be_present
  # end

  # it "contains Offer information", :vcr do
  #   item = @connector.lookup_by_asin('0321584104')
  #   expect(item.raw.Offers).to be_present
  #   expect(item.raw.OfferSummary).to be_present
  # end

  # it "contains EditorialReviews", :vcr do
  #   item = @connector.lookup_by_asin('0321584104')
  #   expect(item.raw.EditorialReviews).to be_present
  # end

  # it "contains SimilarProducts", :vcr do
  #   item = @connector.lookup_by_asin('0321584104')
  #   expect(item.raw.SimilarProducts).to have_at_least(1).item
  # end

  # it "contains BrowseNodes", :vcr do
  #   item = @connector.lookup_by_asin('0321584104')
  #   expect(item.raw.BrowseNodes.BrowseNode).to have_at_least(2).items
  # end


  describe "validations" do
    it { expect(@product).to validate_presence_of(:asin) }
    it { expect(@product).to validate_presence_of(:name) }
    it { expect(@product).to validate_numericality_of(:price) }
    it "can't have a price less than 0" do
      expect(build(:product, price: -1).save).to be_false
    end
  end
  
  describe "instance methods" do
    describe ".fetch_info" do
      it "updates the product's information from Amazon API", :vcr do
        @product = Product.create!(name: "asdf", asin: "0321584104", price: 12.32)
        @product.fetch_info
        expect(@product.name).to start_with('Eloquent Ruby')
      end
    end
  end

  describe "class methods" do

    describe "#fetch_batch" do
      it "returns a SimpleItem", :vcr do
        resp = Product.fetch_batch('0321584104')
        expect(resp[0]).to be_a(ASIN::SimpleItem)
      end

      it "returns multiple SimpleItems", :vcr do
        asins_to_fetch = ['0321584104', '0596516177']
        asins = []
        resp = Product.fetch_batch(asins_to_fetch)
        
        resp.each { |item| asins << item.asin }

        expect(asins).to eq(asins_to_fetch)
        expect(resp).to have(asins_to_fetch.size).things
      end
    end
  end
end
