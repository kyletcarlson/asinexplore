class CreateInitialTables < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :asin, limit: 20
      t.string :name
      t.text :description
      t.float :price
      t.string :url
      t.string :image_url
      t.integer :sales_rank
      t.text :author 
      t.string :binding 
      t.string :ean 
      t.string :edition 
      t.string :isbn 
      t.string :item_height 
      t.string :item_length 
      t.string :item_weight 
      t.string :item_width 
      t.string :label 
      t.string :language 
      t.string :formatted_price 
      t.string :manufacturer 
      t.string :mpn 
      t.string :page_count 
      t.string :part_number 
      t.string :product_group 
      t.string :publication_date 
      t.string :publisher 
      t.string :sku 
      t.string :studio 
      t.string :total_new 
      t.string :total_used 
      t.timestamps
    end

    create_table :searches do |t|
      t.string :keywords
      t.decimal :min_price
      t.decimal :max_price
      t.timestamps
    end
  end
end
