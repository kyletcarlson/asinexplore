# == Schema Information
#
# Table name: searches
#
#  id         :integer          not null, primary key
#  keywords   :string(255)
#  min_price  :integer
#  max_price  :integer
#  created_at :datetime
#  updated_at :datetime
#

class Search < ActiveRecord::Base

  def products
    @products ||= find_products
  end

  private

    def find_products
      products = Product.where("name like ?", "%#{keywords}%") if keywords.present?
      # products = products.where("description like ?", "%#{keywords}%") if keywords.present?
      # products = products.where("price >= ?", min_price) if min_price.present?
      # products = products.where("price <= ?", max_price) if max_price.present?
      products
    end
end
